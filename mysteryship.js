var Mystery_ship = function(params){
    // Захватчик
    var sprite = Main.sprites.sprites_enemy;

    Entity.apply(this, arguments);
    this.speed = 1;
    this.price = 50;
    this.radius = 30;


    conf = {
        name:'move_left',
        sprite: sprite['mistery'],
        action: function(entity, time){
            entity.move(-entity.speed*Math.round(30/60), 0);
            if(entity.x < -30){
                entity.scene.delete_entity(entity);
            }
        }
    }

    var obj_behavior2 = new Behavior(conf);
    this.add_behavior(obj_behavior2);

    var conf = {
        name:'boom',
        sprite: sprite['explosion'],
        action: function(entity, time){
            if(time>12){
                entity.scene.delete_entity(entity);
                // entity.setactive();
            }
        }
    }
    var obj_behavior = new Behavior(conf);
    this.add_behavior(obj_behavior);

    this.set_active_behavior('move_left');
}

extend(Mystery_ship, Entity);

Mystery_ship.prototype.unactive = function(){
    this.scene.score += this.price;
    this.set_active_behavior('boom');
}
