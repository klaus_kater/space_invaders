var Entity = function(action){
    // Задает поведение объекта и отображение
    this.manager = new ManagerEntity();
    this.manager.action = action;
    this.manager.entity = this;
    this.scene = undefined;
    this.behavior = {};
    this.active_behavior = undefined;
    this.x = 0;
    this.y = 0;
    this.radius = 1;
    this.collider_list = [];
    this.active = true;
    this.collide = function() {};
    this.angle = 0;
}

Entity.prototype.unactive = function() {
    this.active = false;
}

Entity.prototype.setactive = function() {
    this.active = true;
}

Entity.prototype.update = function(time, input){
    // Транслируем менеждеру
    this.manager.update(time, input);
}

Entity.prototype.render = function(time, ctx_canvas){
    // Транслируем менеждеру
    this.manager.render(time, ctx_canvas);
}

Entity.prototype.put = function(x, y){
    // Задает объекту прямые координаты
    this.x = x;
    this.y = y;
}

Entity.prototype.move = function(x, y) {
    // двигает объект на х и у
    this.x += x;
    this.y += y;
}

Entity.prototype.rotate = function(angle) {
    this.angle += angle;
}

Entity.prototype.get_pos = function(){
    // возвращает координаты
    return [this.x ,this.y];
}

Entity.prototype.validate = function(behavior){
    // Проверка объекта на целостность
    // по задумке вызывается при добавлении на сцену, для проверки всех необходимых параметров.
    return true;
}

Entity.prototype.add_behavior = function(behavior){
    // Добавляем состояние
    if(this.behavior[behavior.name]){
        return; // Обработка ошибок пока никакая.
    }
    this.behavior[behavior.name] = behavior;
    behavior.entity = this;
}

Entity.prototype.set_active_behavior = function(behavior){
    // Устанавливает активное состояние
    if(this.active_behavior != behavior){
        this.manager.set_behavior(behavior);
        this.active_behavior = behavior;
    }
}

Entity.prototype.get_active_behavior = function(){
    // Возвращает название активного состояния
    return this.manager.get_behavior();
}

Entity.prototype.add_collision = function(collider, action){
    // Добавляет объект с которым возможно столкновение
    this.collider_list.push(collider);
}
