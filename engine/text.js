var Text = function(){
    // Задает поведение объекта и отображение
    this.scene = undefined;
    this.x = 0;
    this.y = 0;
    this.size = 30;
    this.text = '';
}

Text.prototype.validate = function(){
    return true;
}

Text.prototype.update = function(time, input){
    if(this.action){
        this.action();
    }
}

Text.prototype.render = function(time, ctx_canvas){
    ctx_canvas.font = this.size + "px Arial";
    ctx_canvas.fillText(this.text,this.x ,this.y);
}

Text.prototype.put = function(x, y){
    this.x = x;
    this.y = y;
}

Text.prototype.move = function(x, y) {
    this.x += x;
    this.y += y;
}

