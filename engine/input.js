(function() {
    // Бибиотека взята из интернета. Обрабатывает ввод
    var pressedKeys = {};

    function setKey(event, status) {
        var code = event.keyCode;
        var key;

        switch(code) {
        case 32:
            key = 'SPACE'; break;
        case 37:
            key = 'LEFT'; break;
        case 38:
            key = 'UP'; break;
        case 39:
            key = 'RIGHT'; break;
        case 40:
            key = 'DOWN'; break;
        default:
            // Convert ASCII codes to letters
            key = String.fromCharCode(code);
        }

        pressedKeys[key] = status;
    }

    document.addEventListener('keydown', function(e) {
        setKey(e, 'pressed');
        event.stopPropagation();
    });

    document.addEventListener('keyup', function(e) {
        setKey(e, 'release');
        event.stopPropagation();
    });

    window.addEventListener('blur', function() {
        pressedKeys = {};
        event.stopPropagation();
    });

    window.addEventListener('mouse_up', function(e) {
        pressedKeys['MOUSE'] = 'release';
        event.stopPropagation();
    });

    window.input = {
        is_down: function(key){
            return this.status(key) == 'pressed';
        },
        status: function(key) {
            return pressedKeys[key.toUpperCase()];
        },
        finished: function(key){
            pressedKeys[key.toUpperCase()] = undefined;
        }
    };
})();