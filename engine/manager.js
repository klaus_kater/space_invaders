var ManagerEntity = function(){
    // Управляет состояниями объектов
    this.entity = undefined;
    this.action = undefined;
    this.time = 0;
}

ManagerEntity.prototype.update = function(time, input){
    // Состояния могут меняться по 2м причинам, либо внешним (пользователь, колизии), либо внутренним (время).
    // Внешние обрабатывает менеджер, внутренние - сами состояния
    this.time = time;
    if(this.action && this.entity.active){
        this.action(this.entity, time, input);
    }
    if(this.entity.active){
        for(var i=0; i<this.entity.collider_list.length; i++){
            var tmpx = Math.abs(this.entity.collider_list[i].x - this.entity.x);
            var tmpy = Math.abs(this.entity.collider_list[i].y - this.entity.y);
            if(tmpx+tmpy < this.entity.collider_list[i].radius + this.entity.radius){
                this.entity.collide(this.entity.collider_list[i]);
                break;
            }
        }
    }
    this.active_behavior.update(this.entity, time);
}

ManagerEntity.prototype.render = function(time, ctx_canvas){
    // Рисуем объект исходя из его состояния
    var shot = this.active_behavior.get_shot(time);
    
    ctx_canvas.save();
    ctx_canvas.translate(this.entity.x-(shot.width/2),this.entity.y-(shot.height/2));
    ctx_canvas.rotate(shot.angle*Math.PI/180);
    ctx_canvas.drawImage(shot.img,
                    shot.x, shot.y,
                    shot.width, shot.height,
                    0,0,
                    shot.width, shot.height);
    ctx_canvas.translate(0,0);
    ctx_canvas.restore();

}

ManagerEntity.prototype.set_behavior = function(behavior_name){
    // Устанавливаем активным состояние
    this.active_behavior = this.entity.behavior[behavior_name];
    this.active_behavior.begin = this.time;
    this.active_behavior.manager = this;
}

ManagerEntity.prototype.get_behavior = function() {
    // Возвращает имя состояния
    return this.active_behavior.name;
}