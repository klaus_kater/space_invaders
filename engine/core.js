function extend(Child, Parent) {
    // Наследование объектов
    var F = function() { }
    F.prototype = Parent.prototype
    Child.prototype = new F()
    Child.prototype.constructor = Child
    Child.superclass = Parent.prototype
};

function check_require(params, require_params) {
    // Вспомогательная функция, для проверки наличия всех необходимых свойств.
    for(var i = 0; i<require_params.length; i++){
        if(params[require_params[i]] == null){
            return true;
        }
    }
    return false;
}


var Main = {};
Main.init = function(create_scenes){
    var canvas = document.createElement("canvas");
    this.ctx = canvas.getContext("2d");
    this.width_canvas = 600;
    this.height_canvas = 500;
    canvas.width = this.width_canvas;
    canvas.height = this.height_canvas;
    document.body.appendChild(canvas);


    // Хак для поддержки разными браузерами requestAnimationFrame
    window.userRequestAnimationFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
              function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
    })();
    // создаем сцены, добавляем объекты и обработчики действий пользователя
    this.active_scene = create_scenes();
    this.lastRender = new Date();
    this.start_game = this.lastRender;
    this.startcounter = 0;
}

Main.set_active_scene = function(active_scene) {
    this.active_scene = active_scene;
    this.active_scene.begin = this.startcounter;
}

Main.handleInput = function(dt) {
    if(input.isDown('DOWN') || input.isDown('s')) {
        player.pos[1] += playerSpeed * dt;
    }
}
Main.list_fps = [];
for( var i=0; i<100; i++){
    Main.list_fps.push(1);
}
//////////////////////////////////////////////////////////////////////////////////////////
Main.run = function () {
    var delta = new Date() - this.lastRender;
    Main.list_fps.shift();
    Main.list_fps.push(1000/(delta || 1));
    this.lastRender = new Date();

    this.ctx.clearRect(0, 0, this.width_canvas, this.height_canvas);
    //-- изменение модели и отрисовки
    this.active_scene.update(input);
    this.active_scene.render(this.ctx);
    ////////////////////////////////////
    // Немного костылей. отрисовка фпс
    var height_graphik = 40;

    this.ctx.beginPath();
    this.ctx.moveTo(10,10+height_graphik-(Main.list_fps[0]*0.3));
    for(var i=1; i<100; i++){
        this.ctx.lineTo(10+i,10+height_graphik-(Main.list_fps[i]*0.3));// магическая циферка, в одном пикселе 3 единицы графика
    }
    this.ctx.stroke();

    middle_fps = Main.list_fps.reduce(function(sum, current) {
      return sum + current;
    });
    this.ctx.font = "30px Arial";
    this.ctx.fillText(Math.round(middle_fps/100),10,50);

    //-- следующий цикл
    window.userRequestAnimationFrame(function () {
        Main.run();
    });
    this.startcounter++;
}