var Scene = function(){
    // Объект сцены.
    this.entitys = [];
    this.begin = 0;
    this.action = undefined;
}

Scene.prototype.add_entity = function(obj){
    // Добавляем объект на сцену
    if(!obj.validate()){
        return
    }
    this.entitys.push(obj);
    obj.scene = this;
}

Scene.prototype.delete_entity = function(obj){
    // Удалаем объект из сцены
    this.entitys.splice(this.entitys.indexOf(obj), 1);
}

Scene.prototype.update = function(input){
    if(this.action){
        this.action(Main.startcounter - this.begin);
    }
    for(var i = 0; i<this.entitys.length; i++){
        this.entitys[i].update(Main.startcounter - this.begin, input);
    }
}

Scene.prototype.render = function(ctx_canvas){
    // перебираем объекты сцены, и рендерим уже их
    for(var i = 0; i<this.entitys.length; i++){
        this.entitys[i].render(Main.startcounter - this.begin, ctx_canvas);
    }
}
