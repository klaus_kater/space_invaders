var Sprite = function(params){
    if(check_require(params, ['width', 'height', 'x', 'y', 'img'])){
        console.log('error create sprite');
        return 'error';
    }
    this.name = params.name; // Для отладки
    this.width = params.width;
    this.height = params.height;
    this.x = params.x;
    this.y = params.y;
    this.img = params.img;
    this.count = params.count || 1;
}
Sprite.prototype.get_shot = function(num){
    // возвращает координаты и размер текущего кадра на общей канве
    num = num % this.count;
    return [this.x + (this.width*num),this.y, this.width, this.height];
}