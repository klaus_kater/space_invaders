var Behavior = function(params){
    // класс отвечающий за состояние. Обрабатывает поведение в этом состоянии, и отрисовку
    if(check_require(params, ['name', 'sprite'])){
        return 'error';
    }
    this.begin = 0;
    this.manager = undefined;
    this.action = params.action;
    this.name = params.name;
    this.sprite = params.sprite;
}

Behavior.prototype.update = function(entity, time){
    if(this.action){
        this.action(entity, time - this.begin);
    }
}

Behavior.prototype.get_shot = function(time){
    // Получаем текущий кадр из спрайта
    var passed_tick = time - this.begin;
    var coords = this.sprite.get_shot(passed_tick);
    var shot = {
        img:this.sprite.img,
        x:coords[0],
        y:coords[1],
        width:coords[2],
        height:coords[3],
        angle:this.manager.entity.angle,
    };
    return shot;
}

Behavior.prototype.set_behavior = function(name){
    this.begin = Main.megacounter;
    this.manager.set_behavior(name);
}