var Invader = function(params){
    // Захватчик
	var sprite = Main.sprites.sprites_enemy;

    Entity.apply(this, arguments);
    this.speed = 1;
    this.price = 10;
    this.radius = 25;
    var _start_left = false;
    var _start_pos = [0,0];

    this.set_speed = function(){
        this.speed_prepare = this.start_left()?_start_pos[0] - this.x:this.x - _start_pos[0];
    }

    this.start_pos = function(val) {
        if (!arguments.length) return _start_pos;

        _start_pos = val;
        this.set_speed();
    };

    this.start_left = function(newVal) {
        if (!arguments.length) return _start_left;

        var tmpx = newVal?-20:Main.width_canvas+20;
        this.put(tmpx, 0);
        _start_left = newVal;
        this.set_speed();
    }

    var conf = {
        name:'prepare',
        sprite: sprite['one'],
        action: function(entity, time){
            var start_pos = entity.start_pos();
            var speed = 7;
            if(Math.abs(entity.x - start_pos[0]) < speed+1){
                entity.x = start_pos[0];
                entity.set_active_behavior('stand');
            }
            var direction = entity.x > start_pos[0]? -1:1;

            var tmpx = entity.x + (direction * speed);
            var tmpy = Math.round(Math.sqrt(Math.abs((direction>0?tmpx-start_pos[0]:start_pos[0]-tmpx)*200)));
            entity.put(tmpx, start_pos[1] - tmpy);
        }
    }
    var obj_behavior = new Behavior(conf);
    this.add_behavior(obj_behavior);

    var conf = {
        name:'stand',
        sprite: sprite['one'],
    }
    var obj_behavior = new Behavior(conf);
    this.add_behavior(obj_behavior);

    conf = {
        name:'move_left',
        sprite: sprite['one'],
        action: function(entity, time){
            entity.move(-entity.speed*Math.round(30/60), 0);
        }
    }

    var obj_behavior2 = new Behavior(conf);
    this.add_behavior(obj_behavior2);

    conf = {
        name:'move_right',
        sprite: sprite['one'],
        action: function(entity, time){
            entity.move(entity.speed*Math.round(30/60), 0);
        }
    }

    var obj_behavior3 = new Behavior(conf);
    this.add_behavior(obj_behavior3);

    conf = {
        name:'boom',
        sprite: sprite['explosion'],
        action: function(entity, time){
            if(time>12){
                entity.scene.delete_entity(entity);
            }
        }
    }

    var obj_behavior3 = new Behavior(conf);
    this.add_behavior(obj_behavior3);

    this.set_active_behavior('prepare');
}

extend(Invader, Entity);

Invader.prototype.unactive = function(){
    Entity.prototype.unactive.apply(this, arguments);

    this.scene.list_invaders.splice(this.scene.list_invaders.indexOf(this), 1);
    this.scene.score += this.price;
    this.set_active_behavior('boom');

    
}

Invader.prototype.shoot = function(target) {
	var bullet = new Bullet();
    bullet.set_direction(1);
    bullet.put(this.x, this.y+10);
    bullet.add_collision(target);
    this.scene.add_entity(bullet);
}

