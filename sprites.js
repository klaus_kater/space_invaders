function create_sprites(){
    // Спрайт будет в одном файле, тут координаты и размеры
    var sprites_player = {
        'stand':{
            width:50,
            height:50,
            x:50,
            y:100,
            count:1,
        },
        'move_left':{
            width:50,
            height:50,
            x:0,
            y:100,
            count:1,
        },
        'move_right':{
            width:50,
            height:50,
            x:100,
            y:100,
            count:1,
        },
        'explosion':{
            width:49,
            height:50,
            x:0,
            y:0,
            count:12,},
        'empty':{
            width:0,
            height:0,
            x:0,
            y:0,
            count:1,},
    }
    var sprites_bullet = {
        'run':{
            width:8,
            height:16,
            x:0,
            y:150,
            count:2,},
    }
    var sprites_enemy = {
        'one':{
            width:50,
            height:50,
            x:0,
            y:50,
            count:1,
        },
        'two':{
            width:50,
            height:50,
            x:50,
            y:50,
            count:1,
        },
        'three':{
            width:50,
            height:50,
            x:100,
            y:50,
            count:1,
        },
        'four':{
            width:50,
            height:50,
            x:150,
            y:50,
            count:1,
        },
        'mistery':{
            width:50,
            height:50,
            x:250,
            y:50,
            count:1,
        },
        'explosion':{
            width:49,
            height:50,
            x:0,
            y:0,
            count:12,
        },
    }
    var list_sprites_group = {
        'sprites_player':sprites_player,
        'sprites_bullet':sprites_bullet,
        'sprites_enemy':sprites_enemy
    };

    var sprite_pack = Main.sprite_pack;

    var new_list_sprites_group = {}

    for(group in list_sprites_group){
        new_list_sprites_group[group] = {};
        for(sprite in list_sprites_group[group]){
            config = list_sprites_group[group][sprite];
            if(!config)continue;
            config.img = sprite_pack;
            config.name = sprite;
            new_list_sprites_group[group][sprite] = new Sprite(config);
        }
    }
    return new_list_sprites_group;
}
