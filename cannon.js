var Cannon = function(params){
    // Объект пушки. Двигается с пользователем.
    var sprite = Main.sprites.sprites_player;

    Entity.apply(this, arguments);
    this.shoot_time = 0;

    conf = {
        name:'load',
        sprite: sprite['empty'],
        action: function(entity, time){
            if(time>100){
                entity.set_active_behavior('check');
                entity.check = true;
            }
        }
    }
    var obj_behavior = new Behavior(conf);
    this.add_behavior(obj_behavior);

    conf = {
        name:'check',
        sprite: sprite['empty'],
    }

    var obj_behavior2 = new Behavior(conf);
    this.add_behavior(obj_behavior2);

    this.set_active_behavior('load');
}

extend(Cannon, Entity);

Cannon.prototype.shoot = function() {
    if(this.get_active_behavior() == 'check'){
        // Обработка выстрела, создание пули.
        this.set_active_behavior('load');
        var bullet = new Bullet();
        bullet.set_direction(-1);
        bullet.put(this.x, this.y-10);
        for(var i=0; i<this.scene.list_invaders.length; i++){
            bullet.add_collision(this.scene.list_invaders[i]);
        }
        bullet.add_collision(this.scene.mystery);
        this.scene.add_entity(bullet);
    }
}