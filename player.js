var Player = function(params){

	var sprite = Main.sprites.sprites_player;

	this.cannon = new Cannon();
	this.radius = 25;

    ////////////////////////////////////////////////////////////////

    Entity.apply(this, arguments);
    this.speed = 4;

	function action(entity, time, input){ // Функция обработчик состояний и передвижений
        time_sec = time / 60;

        cicle = time_sec % 20;

        if(input.is_down('left')){
            entity.set_active_behavior('move_left');
        }else if(input.is_down('right')){
            entity.set_active_behavior('move_right');
        }else{
            entity.set_active_behavior('stand');
        }
        if(input.is_down('space')){
        	entity.cannon.shoot();
        	// console.log(entity.cannon.get_active_behavior()+'11');
        }

    }

    this.manager.action = action;

    var conf = {
        name:'stand',
        sprite: sprite['stand'],
    }
    var obj_behavior = new Behavior(conf);
    this.add_behavior(obj_behavior);

    conf = {
        name:'move_left',
        sprite: sprite['move_left'],
        action: function(entity, time){
            entity.move(-entity.speed*Math.round(30/60));
        }
    }

    var obj_behavior2 = new Behavior(conf);
    this.add_behavior(obj_behavior2);

    conf = {
        name:'move_right',
        sprite: sprite['move_right'],
        action: function(entity, time){
            entity.move(entity.speed*Math.round(30/60));
        }
    }

    var obj_behavior3 = new Behavior(conf);
    this.add_behavior(obj_behavior3);

    conf = {
        name:'boom',
        sprite: sprite['explosion'],
        action: function(entity, time){
            if(time>12){
                entity.set_active_behavior('stand');
                entity.setactive();
            }
        }
    }

    var obj_behavior3 = new Behavior(conf);
    this.add_behavior(obj_behavior3);

    this.set_active_behavior('stand');
    this.put(100, 450);
    this.cannon.put(100, 430);
}

extend(Player, Entity);

Player.prototype.unactive = function(){
    Entity.prototype.unactive.apply(this, arguments);
    this.set_active_behavior('boom');
    this.scene.lives -= 1;
}

Player.prototype.move = function(val) {
	this.x += val;
	if(this.x < 50){this.x = 50;}
	if(this.x > Main.width_canvas-50){this.x = Main.width_canvas-50;}
	this.cannon.x = this.x;
}