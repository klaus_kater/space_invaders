function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}


function create_scenes(){
    // загружаем пачку спрайтов
    Main.sprites = create_sprites();

    // Обработчик для объекта
    var player = new Player();
    player.active = false;

    var game_scene = new Scene();
    game_scene.add_entity(player);
    game_scene.add_entity(player.cannon);
    game_scene.score = 0;
    game_scene.lives = 3;

    var score = new Text();
    score.text = 'csore: ' + game_scene.score;
    score.put(10, 100);
    game_scene.add_entity(score);

    var lives = new Text();
    lives.text = 'lives: '+ game_scene.lives;
    lives.put(10, 130);
    game_scene.add_entity(lives);


    game_scene.dec_live = function(argument) {
        console.log('minus live');
    }

    var start_game = false;
    function create_invaders(argument) {
        var left_margin = 70;
        var top_margin = 120;
        var width_space = 50;
        var height_space = 50;
        var num_line = 3;
        var num_column = 9;
        var start_position_invaders = [];
        for(var line = 0;line<num_line; line++){
            for(var col=0; col<num_column; col++){
                start_position_invaders.push([col*width_space+left_margin, line*height_space+top_margin])
            }
        }
        var list_invaders = [];
        var num_invader = start_position_invaders.length;
        for(var i=0;i<num_invader; i++){
            list_invaders[i] = new Invader();
            list_invaders[i].start_left(Math.random()>0.5?true:false);
            list_invaders[i].start_pos(start_position_invaders.pop(getRandomInt(start_position_invaders.length-1)));
            game_scene.add_entity(list_invaders[i]);
        }
        game_scene.list_invaders = list_invaders;
        start_game = false;
    }
    create_invaders();
    var mystery = new Mystery_ship();
    game_scene.mystery = mystery;
    mystery.put(-50, 30);
    game_scene.add_entity(mystery);

    
    var speed_player = 10;

    function game_over(){
        game_scene.entitys = [];
        var game_over1 = new Text();
        game_over1.text = 'GAME OVER';
        game_over1.put(300, 100);
        var game_over2 = new Text();
        game_over2.text = 'for new game press f5';
        game_over2.put(280, 130);
        game_scene.add_entity(game_over1);
        game_scene.add_entity(game_over2);
    }

    var scene_action = function(time) {
        if(!start_game){
            if(game_scene.list_invaders.every(function(invader){
                return invader.get_active_behavior() == 'stand';
            })){
                start_game = true;
                player.active = true;
                game_scene.list_invaders.forEach(function(invader){
                    invader.set_active_behavior('move_left');
                })
            }
            return;
        };

        if(game_scene.list_invaders.length == 0){
            // Захватчики кончились. Делаем заново.
            game_scene.score+=100;
            create_invaders();
            return;
        }
        // если кто то из захватчиков близко к краю - поворачиваем.
        if(game_scene.list_invaders.some(function(invader){
            return Main.width_canvas - invader.x < 50;
        })){
            game_scene.list_invaders.forEach(function(invader){
                invader.set_active_behavior('move_left');
            })
        }else if(game_scene.list_invaders.some(function(invader){
            return invader.x < 50;
        })){
            game_scene.list_invaders.forEach(function(invader){
                invader.set_active_behavior('move_right');
            })
        }

        time_sec = time / 60;

        cicle = time_sec % 2;
        if(cicle == 0){
            var x = player.x;
            if(player.get_active_behavior() == 'move_left'){
                x-= speed_player;
            }else if(player.get_active_behavior() == 'move_right'){
                x+= speed_player;
            }
            // берем захватчика над игроком, и стреляем в игрока
            var near_invader = undefined;
            var max_y = 0; // у увеличивается в другую сторону, нужен максимальный
            var min_x = 1000;
            for(var i=0; i<game_scene.list_invaders.length; i++){
                var invader = game_scene.list_invaders[i];
                if((Math.abs(invader.x - x) < min_x) || (Math.abs(invader.x - x) <= min_x && invader.y >= max_y)){
                    max_y = invader.y;
                    min_x = Math.abs(invader.x - x);
                    near_invader = invader;
                }
            }
            if(near_invader){
                near_invader.shoot(player);
            }
            
        }

        cicle = time_sec % 10;
        if(cicle == 9){
            mystery.put(Main.width_canvas + 30, 30);
            mystery.set_active_behavior('move_left');
            game_scene.add_entity(mystery);
            // обработчик попадания..
        }

        score.text = 'score: ' + game_scene.score;
        lives.text = 'lives: '+ game_scene.lives;
        if(game_scene.lives < 0){
            game_over()
        }
    }
    game_scene.action = scene_action;
    return game_scene;
}
var sprite_pack = new Image();
sprite_pack.onload = function() {
    Main.sprite_pack = sprite_pack;
    Main.init(create_scenes);
    Main.run();
};
sprite_pack.src = 'sprite_pack.png';

