var Bullet = function(params){
    // Объект пули
    var sprite = Main.sprites.sprites_bullet;

    Entity.apply(this, arguments);
    this.speed = 4;
    var direction = 1;
    this.angle = 0;
    this.radius = 15;
    this.set_direction = function(val){
        direction = val;
        this.angle = direction>0?180:0;
    }
    this.get_direction = function(){
        return direction;
    }

    function action(entity, time, input){

    }

    this.manager.action = action;

    var conf = {
        name:'run',
        sprite: sprite['run'],
        action: function(entity, time){
            entity.move(0, entity.speed*entity.get_direction());
            if(entity.y > Main.height_canvas || entity.y <0){
                entity.scene.delete_entity(entity);
            }
        }
    }
    var obj_behavior = new Behavior(conf);
    this.add_behavior(obj_behavior);

    this.set_active_behavior('run');

    function action_collide(target) {
        target.unactive();
        this.scene.delete_entity(this);
    }

    this.collide = action_collide;
}

extend(Bullet, Entity);
